/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

///
/// General-purpose view creation algorithm <bwynne@cern.ch>
///

#include "EventViewCreatorAlgorithmWithJets.h"
#include "AthLinks/ElementLink.h"
#include "AthViews/ViewHelper.h"
#include "AthViews/View.h"
#include "TrigCompositeUtils/TrigCompositeUtils.h"
#include "TrigCompositeUtils/HLTIdentifier.h"

using namespace TrigCompositeUtils;

EventViewCreatorAlgorithmWithJets::EventViewCreatorAlgorithmWithJets( const std::string& name, ISvcLocator* pSvcLocator )
  : EventViewCreatorAlgorithm( name, pSvcLocator ) {}

EventViewCreatorAlgorithmWithJets::~EventViewCreatorAlgorithmWithJets() {}

StatusCode EventViewCreatorAlgorithmWithJets::initialize() {

  ATH_CHECK( EventViewCreatorAlgorithm::initialize() );
  ATH_CHECK( m_inViewJets.initialize() );

  return StatusCode::SUCCESS;
}

StatusCode EventViewCreatorAlgorithmWithJets::execute( const EventContext& context ) const {
  // create the output decisions from the input collections
  SG::WriteHandle<DecisionContainer> outputHandle = createAndStore( decisionOutputs(), context );
  ATH_CHECK (decisionInputToOutput(context, outputHandle));

  // make the views
  auto viewsHandle = SG::makeHandle( m_viewsKey, context );
  auto viewVector1 = std::make_unique< ViewContainer >();
  ATH_CHECK( viewsHandle.record(  std::move( viewVector1 ) ) );
  auto viewVector = viewsHandle.ptr();
  unsigned int viewCounter = 0;

  //map all RoIs that are stored
  std::vector <ElementLink<TrigRoiDescriptorCollection> > RoIsFromDecision;

  if( not outputHandle.isValid() ) {
    ATH_MSG_ERROR( "Got no decisions from output "<< outputHandle.key() << " because handle not valid");
    return StatusCode::FAILURE;
  }
  if( outputHandle->size() == 0){ // input filtered out
    ATH_MSG_ERROR( "Got no decisions from output "<< outputHandle.key()<<": handle is valid but container is empty. Is this expected?");
    return StatusCode::FAILURE;
  }

  ATH_MSG_DEBUG( "Got output "<< outputHandle.key()<<" with " << outputHandle->size() << " elements" );
  // loop over output decisions in container of outputHandle, follow link to inputDecision
  for ( auto outputDecision : *outputHandle){ 
    ElementLinkVector<DecisionContainer> inputLinks = getLinkToPrevious(outputDecision);
    // loop over input links as predecessors
    for (auto input: inputLinks){
      const Decision* inputDecision = *input;

      // Retrieve jets ...
      ATH_MSG_DEBUG( "Checking there are jets linked to decision object" );
      TrigCompositeUtils::LinkInfo< xAOD::JetContainer > jetELInfo = TrigCompositeUtils::findLink< xAOD::JetContainer >( inputDecision,TrigCompositeUtils::featureString() );
      ATH_CHECK( jetELInfo.isValid() );
      const xAOD::Jet *jet = *jetELInfo.link;
      ATH_MSG_DEBUG( "Placing xAOD::JetContainer " );
      ATH_MSG_DEBUG( "   -- pt="<< jet->p4().Et() <<" eta="<< jet->eta() << " phi="<< jet->phi() );
      
      // find the RoI
      auto roiELInfo = TrigCompositeUtils::findLink<TrigRoiDescriptorCollection>( inputDecision, m_roisLink.value() );
      auto roiEL = roiELInfo.link;
      ATH_CHECK( roiEL.isValid() );
      // check if already found
      auto roiIt=find(RoIsFromDecision.begin(), RoIsFromDecision.end(), roiEL);
      if ( roiIt == RoIsFromDecision.end() ){
        RoIsFromDecision.push_back(roiEL); // just to keep track of which we have used
        const TrigRoiDescriptor* roi = *roiEL;
        ATH_MSG_DEBUG("Found RoI:" <<*roi<<" FS="<<roi->isFullscan());
        ATH_MSG_DEBUG( "Positive decisions on RoI, preparing view" );
        
        // make the view
        ATH_MSG_DEBUG( "Making the View" );
        auto newView = ViewHelper::makeView( name()+"_view", viewCounter++, m_viewFallThrough ); //pointer to the view

        // Use a fall-through filter if one is provided
        if ( m_viewFallFilter.size() ) {
          newView->setFilter( m_viewFallFilter );
        }

        viewVector->push_back( newView );
        ATH_CHECK( linkViewToParent( inputDecision, viewVector->back() ) );

        // link decision to this view

        outputDecision->setObjectLink( TrigCompositeUtils::viewString(), ElementLink< ViewContainer >(m_viewsKey.key(), viewVector->size()-1 ));//adding view to TC
        ATH_MSG_DEBUG( "Adding new view to new decision; storing view in viewVector component " << viewVector->size()-1 );
        ATH_CHECK( placeRoIInView( roiEL, viewVector->back(), context ) );
        ATH_CHECK( placeJetInView( jet, viewVector->back(), context ) );
      }
      else {
        int iview = roiIt-RoIsFromDecision.begin();
        outputDecision->setObjectLink( TrigCompositeUtils::viewString(), ElementLink< ViewContainer >(m_viewsKey.key(), iview ) ); //adding view to TC
        ATH_MSG_DEBUG( "Adding already mapped view " << iview << " in ViewVector , to new decision");
      }
    }// loop over previous inputs
  } // loop over decisions

  ATH_MSG_DEBUG( "Launching execution in " << viewVector->size() << " views" );
  ATH_CHECK( ViewHelper::scheduleViews( viewVector,           // Vector containing views
                                        m_viewNodeName,       // CF node to attach views to
                                        context,              // Source context
                                        getScheduler(),
                                        m_reverseViews ) );
  
  // store views
  // auto viewsHandle = SG::makeHandle( m_viewsKey );
  // ATH_CHECK( viewsHandle.record(  std::move( viewVector ) ) );
  ATH_MSG_DEBUG( "Store "<< viewsHandle->size() <<" Views");
  if (msgLvl(MSG::DEBUG)) debugPrintOut(context, outputHandle);

  return StatusCode::SUCCESS;
}

StatusCode EventViewCreatorAlgorithmWithJets::placeJetInView( const xAOD::Jet* theObject, SG::View* view, const EventContext& context ) const {
  // fill the Jet output collection
  ATH_MSG_DEBUG( "Adding Jet To View : " << m_inViewJets.key() );
  auto oneObjectCollection = std::make_unique< ConstDataVector< xAOD::JetContainer > >();
  oneObjectCollection->clear( SG::VIEW_ELEMENTS );
  oneObjectCollection->push_back( theObject );

  //store in the view
  auto handle = SG::makeHandle( m_inViewJets,context );
  ATH_CHECK( handle.setProxyDict( view ) );
  ATH_CHECK( handle.record( std::move( oneObjectCollection ) ) );
  return StatusCode::SUCCESS;
}

